/**!
* Ew 官网主页 Js 主框架文件
* delovt (http://sys7em.info)
* 2016-04-28
*/
$(function() {
    $("#ewMenu li").each(menuListen);
    // $(".main").blurr({"sharpness":96});
    $(".libMain").fullpage();
    $(".libMenu").each(libMenuListen);
});

// 导航菜单监听
function menuListen() {
    var _this = this;
    $(_this).hover(
        function(){
            $(_this).css("border-bottom","2px solid #DE6463");
        }, // In
        function(){
            $(_this).css("border-bottom","2px solid #F9F9F9");
        } // Out
    );
}
// 内容菜单监听
function libMenuListen() {
    var _this = this;
    $(_this).hover(
        function() {
            $(_this).css({
                "background-color": "#fff",
                "color": "#000"
            });
        },
        function() {
            $(_this).css({
                "background-color": "",
                "color": "#fff"
            });
        }
    );
    $(_this).popover({
        title: " ",
        html: true,
        placement: "bottom",
        content: getLibCont(_this),
    });
}
// 内容获取
function getLibCont(item) {
    var _this = $(item).attr("cont");
    var cont = $(_this).html();
    return cont;
}
