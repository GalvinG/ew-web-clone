/**!
* Ew 官网主页 Js 主框架文件
* delovt (http://sys7em.info)
* 2016-04-28
*/
$(function() {
    $("#ewMenu li").each(menuListen);
    $.noConflict();
});

function monitorClassChg() {
    var str = '<!--\r\n\u0020\u0020\u9e21\u4e1d\u9a97\u4e86\u6211\u4eec\u0031\u0030\u0030\u0077\u002c\u5e26\u7740\u4ed6\u7684\u88f8\u5954\u89c6\u9891\u8dd1\u5566\u0021\u0020\u6211\u4eec\u6ca1\u6709\u6ca1\u6709\u529e\u6cd5\u002c\u53ea\u597d\u8fd9\u6837\u50ac\u5de5\u8d44\u002c\u0020\u539f\u4ef7\u90fd\u662f\u4e00\u767e\u591a\u4e24\u767e\u591a\u002c\u73b0\u5728\u901a\u901a\u53ea\u8981\u4e8c\u5341\u5757\u002c\u901a\u901a\u53ea\u8981\u4e8c\u5341\u5757\u0021\u0021\u8be6\u60c5\u8bf7\u54a8\u8be2\u0051\u0051\u7fa4\u003a\u0032\u0034\u0039\u0034\u0036\u0038\u0031\u0032\u0038!\r\n-->';
    str = "<div>"+str+"</div>";
    return str;
}

function menuListen() {
    var _this = this;
    $(_this).hover(
        function(){
            $(_this).css("border-bottom","2px solid #DE6463");
        }, // In
        function(){
            $(_this).css("border-bottom","");
        } // Out
    );
}
